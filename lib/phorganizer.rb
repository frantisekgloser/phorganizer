require_relative "phorganizer/version"
require "find"
require "exiftool_vendored"
require "fileutils"

module Phorganizer

  HOMEDIR = Dir.home

  JPGS = ["jpg", "jpeg"]
  TIFS = ["exif", "tif", "tiff"]
  PHOTO = JPGS+TIFS+["bmp", "bpg", "cd5", "cdr", "cpt", "dwf", "dwg", "dxf", "ecw", "gif", "ico", "img", "pam", "pbm", "pcx", "pgf", "pgm", "png", "pnm", "ppm", "psd", "psp", "sgi", "sid", "tga"]
  VIDEO = ["3g2", "3gp", "asf", "amv", "avi", "drc", "f4a", "f4b", "f4p", "f4v", "flv", "gifv", "m2v", "m4p", "m4v", "mkv", "mng", "mov", "mp2", "mp4", "mpe", "mpeg", "mpg", "mpv", "mxf", "nsv", "ogg", "ogv", "qt", "rm", "rmvb", "roq", "svi", "vob", "webm", "wmv", "yuv"]


  def self.valid_dirs?(directory, output, add)
    valid = true
    if !Dir.exists?(directory)
      valid = false
      puts ("Input directory " + directory + " doesn't exist!!!")
    elsif !add && Dir.exists?(output) 
      valid = false
      puts ("Output directory " + output + " exists!!!")
    end
    valid
  end
  private_class_method :valid_dirs?

  def self.set_files(directory, output, list)
    files = []
    if list=='all'
      files = Dir.glob("#{directory}/**/*").find_all { |file| (File.file?(file) && !(file.include?(output[1..-1]))) }
    else
      files = Dir.glob("#{directory}/**/*").find_all { |file| (File.file?(file) && list.include?(File.extname(file).downcase[1..-1]) && !(file.include?(output[1..-1]))) }
    end  
  end
  private_class_method :set_files

  def self.set_arg(file)
    begin
      args = Exiftool.new(file).to_hash.keep_if { |key, value| ((key.to_s.include?("date")) && (value.class==Date || value.class==Time)) }
      args.sort_by { |key, value| value.to_datetime }
      args.to_a[0][1].localtime
    rescue
      args = File.ctime(file)
    end
  end
  private_class_method :set_arg

  def self.set_path(output, arg, sort)
    case sort  
      when 'year'
        path = output+"/"+arg.year.to_s.rjust(4,"0")+"/"
      when 'month'
        path = output+"/"+arg.year.to_s.rjust(4,"0")+arg.month.to_s.rjust(2,"0")+"/"
      when 'day'
        path = output+"/"+arg.year.to_s.rjust(4,"0")+arg.month.to_s.rjust(2,"0")+arg.day.to_s.rjust(2,"0")+"/"
    end
    FileUtils.mkdir_p(path)
    path
  end
  private_class_method :set_path

  def self.set_name(file, arg)
    name = arg.year.to_s.rjust(4,"0")+arg.month.to_s.rjust(2,"0")+arg.day.to_s.rjust(2,"0")+arg.hour.to_s.rjust(2,"0")+arg.min.to_s.rjust(2,"0")+arg.sec.to_s.rjust(2,"0")+"_"+File.basename(file, ".*")
  end
  private_class_method :set_name

  def self.set_filename(file, output, sort)
    arg=set_arg(file)
    path = set_path(output, arg, sort)
    name = set_name(file, arg) 
    filename = path+name+"."+File.extname(file).downcase[1..-1]
    i = 0
    while File.exists?(filename) do
      filename = path+name+"__"+i.to_s.rjust(4,"0")+"."+File.extname(file).downcase[1..-1]
      i+=1
    end  
    filename
  end
  private_class_method :set_filename
  
  def self.add_to_phorganized(files, output, sort)
    number_of_files = files.count
    files.each do |file|
      puts("I'm working on file #{file}")
      filename = set_filename(file, output, sort)
      FileUtils.cp file, filename
      puts ("#{files.index(file)+1} of #{number_of_files} done till now." )
    end
  end
  private_class_method :add_to_phorganized

  def self.organize(directory: HOMEDIR, output: HOMEDIR+"/phorganized", list: PHOTO+VIDEO, sort: 'year', add: false)
    puts()
    if valid_dirs?(directory, output, add) 
      puts("Preparing list of files for phorganizing...")
      files = set_files(directory, output, list)
      add_to_phorganized(files, output, sort)
    end
    files
  end
    
end