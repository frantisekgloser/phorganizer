require 'spec_helper'
require_relative '../../lib/phorganizer'
require 'fileutils'

include Phorganizer

RSpec.describe 'Phorganizer' do

  NOT_EXISTING_DIRECTORY = Phorganizer::HOMEDIR+"/zyx9876543210xyz"

  let(:add) { false }
  let(:arg) { testtime }
  let(:directory) { Phorganizer::HOMEDIR }
  let(:output) { NOT_EXISTING_DIRECTORY }
  let(:pathy) { output+"/"+testtime.year.to_s.rjust(4,"0")+"/" }
  let(:pathm) { output+"/"+testtime.year.to_s.rjust(4,"0")+testtime.month.to_s.rjust(2,"0")+"/" }
  let(:pathd) { output+"/"+testtime.year.to_s.rjust(4,"0")+testtime.month.to_s.rjust(2,"0")+testtime.day.to_s.rjust(2,"0")+"/" }
  let(:sort) { 'year' }
  let(:testdir) { [directory+"/testjpg.jpg", directory+"/testtif.tif", directory+"/testbmp.bmp", directory+"/testico.ico", directory+"/test3gp.3gp", directory+"/testavi.avi", directory+"/testtxt.txt", directory+"/testxxx.xxx"] }
  let(:testtime) { Time.now }
  let(:file) { testdir[0] }
  let(:partname) { testtime.year.to_s.rjust(4,"0")+testtime.month.to_s.rjust(2,"0")+testtime.day.to_s.rjust(2,"0")+testtime.hour.to_s.rjust(2,"0")+testtime.min.to_s.rjust(2,"0")+testtime.sec.to_s.rjust(2,"0")+"_" }
    
  
  describe '#valid_dirs?' do

    subject { Phorganizer.send(:valid_dirs?, directory, output, add) }

    context " if input directory doesn't exist and output directory exists" do
      let(:output) { directory }
      let(:directory) { NOT_EXISTING_DIRECTORY }

      it { expect(subject).to eql false}
    end

    context " if input and output directories don't exist" do
      let(:directory) { NOT_EXISTING_DIRECTORY }
      
      it { expect(subject).to eql false}
    end

    context " if input and output directories exist" do
      let(:output) { directory }

      context " and add is not set" do
        

        it { expect(subject).to eql false}
      end

      context " and add is set" do
        let(:add) { true }

        it { expect(subject).to eql true}
      end

    end

    context " if input directory exist and output directory doesn't exist" do
      
      context " and add is not set" do
 
        it { expect(subject).to eql true}
      end
      
      context " and add is set" do
        let(:add) { true }

        it { expect(subject).to eql true}
      end

    end

  end

  describe '#set_files' do
    
    subject { Phorganizer.send(:set_files, directory, output, list) }

    context " for list all" do
      let(:list) { 'all' }

      before do
        allow(Dir).to receive(:glob).with("#{directory}/**/*").and_return(testdir)
        allow(File).to receive(:file?).and_return(true)
      end

      it { expect(subject).to eql testdir }
    end

    context ' for list ["bmp"]' do
      let(:list) { ["bmp"] }
      
      before do
        allow(Dir).to receive(:glob).with("#{directory}/**/*").and_return(testdir)
        allow(File).to receive(:file?).and_return(true)
      end

      it { expect(subject).to eql [directory+"/testbmp.bmp"] }
    end

    context ' for list ["jpg", "tif"]' do
      let(:list) { ["jpg", "tif"] }
      
      before do
        allow(Dir).to receive(:glob).with("#{directory}/**/*").and_return(testdir)
        allow(File).to receive(:file?).and_return(true)
      end

      it { expect(subject).to eql [directory+"/testjpg.jpg", directory+"/testtif.tif"] }
    end

  end
  
  describe '#set_arg' do

    subject { Phorganizer.send(:set_arg, file) }

    context " for the 1st file from testdir" do

      before do
        allow(File).to receive(:ctime).with(file).and_return(testtime)
      end

      it { expect(subject).to eql testtime }
    end

    context " for the 2nd file from testdir" do
      let(:file) { testdir[1] }

      before do
        allow(File).to receive(:ctime).with(file).and_return(testtime+1)
      end

      it { expect(subject).to eql testtime+1 }
    end

    context " for the 3rd file from testdir" do
      let(:file) { testdir[2] }

      before do
        allow(File).to receive(:ctime).with(file).and_return(testtime+2)
      end

      it { expect(subject).to eql testtime+2 }
    end

  end

  describe '#set_path' do

    subject { Phorganizer.send(:set_path, output, arg, sort) }

    context "sort to years" do

      before do
        allow(FileUtils).to receive(:mkdir_p).with(pathy)
      end

      it { expect(subject).to eql pathy }
    end

    context "sort to months" do
      let(:sort) { 'month' }
   
    
      before do
        allow(FileUtils).to receive(:mkdir_p).with(pathm)
      end

      it { expect(subject).to eql pathm }
    end

    context "sort to days" do
      let(:sort) { 'day' }
   
      before do
        allow(FileUtils).to receive(:mkdir_p).with(pathd)
      end

      it { expect(subject).to eql pathd }
    end

  end

  describe '#set_name' do

    subject { Phorganizer.send(:set_name, file, arg) }

    context " for the 1st file from testdir" do

      it { expect(subject).to eql partname+File.basename(file, ".*") }
    end

    context " for the 2nd file from testdir" do
      let(:file) { testdir[1] }

      it { expect(subject).to eql partname+File.basename(file, ".*") }
    end

    context " for the 3rd file from testdir" do
      let(:file) { testdir[2] }

      it { expect(subject).to eql partname+File.basename(file, ".*") }
    end

  end

  describe '#set_filename' do
    let(:filename) { pathy+partname+File.basename(file) }
    let(:filename0) { pathy+partname+File.basename(file, ".*")+"__0000"+File.extname(file) }
    let(:filename1) { pathy+partname+File.basename(file, ".*")+"__0001"+File.extname(file) }

    subject { Phorganizer.send(:set_filename, file, output, sort) }

    context " for the 1st occurence of the 1st file from testdir" do

      before do
        allow(Phorganizer).to receive(:set_arg).with(file).and_return(testtime)
        allow(FileUtils).to receive(:mkdir_p).with(pathy)        
        allow(File).to receive(:exists?).with(filename).and_return(false)
      end

      it { expect(subject).to eql filename }
    end

    context " for the 2nd occurence of the 1st file from testdir" do
      
      before do
        allow(Phorganizer).to receive(:set_arg).with(file).and_return(testtime)
        allow(FileUtils).to receive(:mkdir_p).with(pathy)
        allow(File).to receive(:exists?).with(filename).and_return(true)
        allow(File).to receive(:exists?).with(filename0).and_return(false)
      end

      it { expect(subject).to eql filename0 }
    end

    context " for the 3rd occurence of the 1st file from testdir" do
      
      before do
        allow(Phorganizer).to receive(:set_arg).with(file).and_return(testtime)
        allow(FileUtils).to receive(:mkdir_p).with(pathy)
        allow(File).to receive(:exists?).with(filename).and_return(true)
        allow(File).to receive(:exists?).with(filename0).and_return(true)
        allow(File).to receive(:exists?).with(filename1).and_return(false)
      end

      it { expect(subject).to eql filename1 }
    end

    context " for the 2nd file from testdir" do
      let(:file) { testdir[1] }
      let(:filename) { pathy+partname+File.basename(file) }

      before do
        allow(Phorganizer).to receive(:set_arg).with(file).and_return(testtime)
        allow(FileUtils).to receive(:mkdir_p).with(pathy)        
        allow(File).to receive(:exists?).with(filename).and_return(false)
      end

      it { expect(subject).to eql filename }
    end

  end

  describe '#add_to_phorganized' do
    let(:files) { [file] }

    subject { Phorganizer.send(:add_to_phorganized, files, output, sort) }

    context " with 1st file from testdir" do
      
      before do
        allow(Exiftool).to receive(:new).with(file).and_return(testtime)
        allow(File).to receive(:ctime).with(file).and_return(testtime)
        allow(FileUtils).to receive(:mkdir_p).with(pathy)
        allow(FileUtils).to receive(:cp)        
      end
      it { expect(subject).to eql files }
    end

    context " with 2nd file from testdir" do
      let(:file) { testdir[1] }
      let(:files) { [file] }

      before do
        allow(Exiftool).to receive(:new).with(file).and_return(testtime)
        allow(File).to receive(:ctime).with(file).and_return(testtime)
        allow(FileUtils).to receive(:mkdir_p).with(pathy)
        allow(FileUtils).to receive(:cp)        
      end
      it { expect(subject).to eql files }
    end

    context " with 3rd file from testdir" do
      let(:file) { testdir[2] }
      let(:files) { [file] }

      before do
        allow(Exiftool).to receive(:new).with(file).and_return(testtime)
        allow(File).to receive(:ctime).with(file).and_return(testtime)
        allow(FileUtils).to receive(:mkdir_p).with(pathy)
        allow(FileUtils).to receive(:cp)       
      end
      it { expect(subject).to eql files }
    end

  end

  describe '#organize' do
    let (:list) { ["bmp", "3gp"] }

    subject { Phorganizer::organize(directory: directory, output: output, list: list, sort: sort, add: add) }

    context " with testdir" do
    
      before do
        allow(Phorganizer).to receive(:valid_dirs?).and_return(true)        
        allow(Phorganizer).to receive(:set_files).with(directory, output, list).and_return(testdir)
        allow(Phorganizer).to receive(:add_to_phorganized).with(testdir, output, sort).and_return(testdir)     
      end

      it { expect(subject).to eql testdir }
    end

    context " with testdir" do
              
      before do
        allow(Phorganizer).to receive(:valid_dirs?).and_return(true)        
        allow(Phorganizer).to receive(:set_files).with(directory, output, list).and_return(testdir)
        allow(Phorganizer).to receive(:add_to_phorganized).with(testdir, output, sort).and_return(testdir)     
      end

      it { expect(subject).to eql testdir }
    end

  end

end