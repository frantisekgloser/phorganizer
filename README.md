# Phorganizer

Phorganizer is a tool for automatic organizing of photos. It takes chosen files from source directory and copies it to destination organized with given system. 

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'phorganizer'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install phorganizer

## Usage

Whole procedure is runnable from IRB with commands 'require "phorganizer"' and 'Phorganizer::organize(args)'.

Keyword arguments are optional. With default values it can be run without (args) specification ('Phorganizer::organize'). In other cases only arguments changed from default have to be written (e.g. 'Phorganizer::organize(output: "/home/user/my_organized", list: PHOTO+VIDEO+["mp3"])') for change output directory, exclude video files and add organized files to existing directory if it exist.

Arguments list with options and default values: 

	directory:	- source directory with files for organizing
				- tool needs existing directory!
				- default value is home directory (Dir.home)

	output:		- output directory for organized files
				- output can be existing directory if "add:" argument is set to TRUE, 
					otherwise it must be unexisting directory!
				- default value is "phorganized" in home directory (Dir.home + "/phorganized") 

	list:		- list of file types included to organizing?
				- array of file extensions
				- usable constants:
						  
						  JPGS = ["jpg", "jpeg"]
  						
  						TIFS = ["exif", "tif", "tiff"]
  						
  						PHOTO = JPGS+TIFS+["bmp", "bpg", "cd5", "cdr", "cpt", "dwf", "dwg", 
  						"dxf", "ecw", "gif", "ico", "img", "pam", "pbm", "pcx", "pgf", "pgm", 
  						"png", "pnm", "ppm", "psd", "psp", "sgi", "sid", "tga"]
  						
  						VIDEO = ["3g2", "3gp", "asf", "amv", "avi", "drc", "f4a", "f4b", "f4p", 
  						"f4v", "flv", "gifv", "m2v", "m4p", "m4v", "mkv", "mng", "mov", "mp2", 
  						"mp4", "mpe", "mpeg", "mpg", "mpv", "mxf", "nsv", "ogg", "ogv", "qt", 
  						"rm", "rmvb", "roq", "svi", "vob", "webm", "wmv", "yuv"]

				- default value is PHOTO+VIDEO

	sort:		- key for organizing files
				- default value is "year", other possible values are 
					"month" and "day"

					with 'year' it organizes files to subdirs named by year of date 
						(e.g. "./phorganized/1999", "./phorganized/2013", 
						"./phorganized/2014",...)
					with 'month' it organizes files to subdirs named
						by "year+month" of date (e.g. "./phorganized/199903", 
						"./phorganized/201301",	"./phorganized/201302", 
						"./phorganized/201311",	"./phorganized/201402",...)
					with 'day' it organizes files to subdirs named 
						by "year+month+day" of date 
						(e.g. "./phorganized/19990301", "./phorganized/19990305", 
						"./phorganized/20130129", "./phorganized/20130131",...)
	
	add:		- add to existing organized directory?
				- default value is FALSE
				- if FALSE, 'output' must be unexisting directory to get result
				- set it to true if you need to add organized files 
					from another source (e.g. if you make organized
					structure from files stored od DVDs)

Phorganizer can organize all files. Files with correct metadata information (exif, id3,..) are organized with metadata, other files with File.ctime (https://ruby-doc.org/core-2.2.0/File.html#method-c-ctime).
	
## Development

Metadata information management made via exiftool_vendored gem (https://rubygems.org/gems/exiftool_vendored).

## Contributing

Bug reports, pull requests, comments, new ideas or cooperation offers are welcome on GitHub at https://bitbucket.org/frantisekgloser/phorganizer or on e-mail frantisek.gloser@gmail.com.